
//SPAGHETTI CODE - code is not organized enough that it becomes hard to work with.

//encapsulation - method for organizing related info(prop) and behavior (methods) to belong to a single entity.




//create student one
// let studentOneName = 'Tony';
// let studentOneEmail = 'starksindustries@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Peter';
// let studentTwoEmail = 'spideyman@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Wanda';
// let studentThreeEmail = 'scarlettMaximoff@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Steve';
// let studentFourEmail = 'captainRogers@mail.com';
// let studentFourGrades = [91, 89, 92, 93];


// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }





//Encapsulate the following info into 4 student objects using object literals

let studentOne = {
	name: "Tony",
	email: "starkindustrues@gmail.com",
	grades: [89, 84, 78, 88],

	//add functionalities available to a student as object methods
		//keyword "this" refers to the object encapsulating the method where the 'this' is called
	
	login(){
		console.log(`${this.email} has logged in`);
	}, 

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		})
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},


	willPass(){
		//you can call methods inside an object
		return this.computeAve() >= 85 ? true : false
	}, 


// ===== OR =======
	/*
	if(this.computeAve() >= 85){
	  return true;
	} else {
	  return false;
	}

	*/


	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ?true : false
	}

}

console.log(`student one's name is ${studentOne.name}`);

console.log(this); //result: global window object



//Mini- activity [encapsulate properties and methods for student 2, 3, & 4]



// ===== STUDENT 2 =======

let studentTwo = {
	name: "Peter",
	email: "spideyman@gmail.com",
	grades: [78, 82, 79, 85],

	//add functionalities available to a student as object methods
		//keyword "this" refers to the object encapsulating the method where the 'this' is called
	
	login(){
		console.log(`${this.email} has logged in`);
	}, 

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		})
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},


	willPass(){
		//you can call methods inside an object
		return this.computeAve() >= 85 ? true : false
	}, 


	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ?true : false
	}

}

console.log(`student two's name is ${studentTwo.name}`);




// ===== STUDENT 3 =======

let studentThree = {
	name: "Wanda",
	email: "scarlettMaximoff@mail.com",
	grades: [87, 89, 91, 93],

	//add functionalities available to a student as object methods
		//keyword "this" refers to the object encapsulating the method where the 'this' is called
	
	login(){
		console.log(`${this.email} has logged in`);
	}, 

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		})
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},


	willPass(){
		//you can call methods inside an object
		return this.computeAve() >= 85 ? true : false
	}, 

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ?true : false
	}

}

console.log(`student three's name is ${studentThree.name}`);




// ===== STUDENT 4 =======

let studentFour = {
	name: "Steve",
	email: "captainRogers@mail.com",
	grades: [91, 89, 92, 93],

	//add functionalities available to a student as object methods
		//keyword "this" refers to the object encapsulating the method where the 'this' is called
	
	login(){
		console.log(`${this.email} has logged in`);
	}, 

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades){
		this.grades.forEach(grade => {
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		})
	},

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},


	willPass(){
		//you can call methods inside an object
		return this.computeAve() >= 85 ? true : false
	}, 


	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ?true : false
	}

}

console.log(`student four's name is ${studentFour.name}`);








//==================== SECTION =================

const classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors()){ //if(true)
				result++;
			}
		})
		return result;
	},



//================= FUNCTION CODING ACTIVITY ==================

//1. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

	honorsPercentage (){
	return (this.countHonorStudents() / this.students.length *100);

	},

//2.  Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
	
	retrieveHonorStudentInfo(){
		let honors=[];
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				honors.push({
					email: student.email,
					aveGrade: student.computeAve()
				});
			}
		})
		return honors;

	},



//3.  Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

	sortHonorStudentsByGradeDesc(){
		return this.retrieveHonorStudentInfo().sort((a, b) => b.aveGrade-a.aveGrade);
	}

};










// Mini-Quiz:
/*
    1. What is the term given to unorganized code that's very hard to work with? >> spaghetti code
   

   2. How are object literals written in JS? >> {}
  

   3. What do you call the concept of organizing information and functionality to belong to an object? >. encapsulation
   

   4. If student1 has a method named enroll(), how would you invoke it? student1.enroll();
  

   5. True or False: Objects can have objects as properties. >> true
  

   6. What does the this keyword refer to if used in an arrow function method? >> global window object (outside)
   

   7. True or False: A method can have no parameters and still work. >> true


   8. True or False: Arrays can have objects as elements.
    >> true

   9. True or False: Arrays are objects.
    >. true

   10. True or False: Objects can have arrays as properties. >> true

*/
























